# Data download

Many (if not all) PaN facilities provide mechanisms that allow for a dataset to be download.  This may be direct (i.e., via the webbrowser) or using some transfer service (e.g., Globus).

Perhaps the most obvious use-case is to allow researchers to analyse the dataset within the confines of their own facility, whether they be researchers who have used the facility directly or people who have discovered the dataset and would like to understand it better.  Other use-cases could involve automated access or workflows.

Currently, there is no common approach between different PaN facilities on how to package up the data: in which (archival) format are the files stored, where (within the archive) is data placed, where to locate ancillary data (for example, callibration data), etc.

Similarly there is no common approach on how to embed metadata within that download.  Although some file formats (e.g., HDF5/NeXus) support rich metadata, not all formats support this.  Moreover, additional metadata (metadata that is not available in any of the files) may be stored in the dataset catalogue.  It may be helpful to include this metadata to the research when they download a dataset.

This project exists to track the progress of establishing a standard approach (to as greater extent as makes sense) on what the user receives when they choose to download a dataset.
